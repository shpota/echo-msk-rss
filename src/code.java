@SpringBootApplication
public class RssParserApplication {

    private static final String TEMPLATE =
            "<item>\n" +
                    "    <title>\n" +
                    "        TITLE\n" +
                    "    </title>\n" +
                    "    <link>LINK</link>\n" +
                    "    <description></description>\n" +
                    "    <author>info@echo.msk.ru (Эхо Москвы)</author>\n" +
                    "    <guid>DOWNLOAD_URL</guid>\n" +
                    "    <enclosure url=\"DOWNLOAD_URL\"\n" +
                    "               length=\"LENGTH\" type=\"audio/mpeg\"/>\n" +
                    "    <itunes:duration>DURATION</itunes:duration>\n" +
                    "    <itunes:explicit>No</itunes:explicit>\n" +
                    "    <pubDate>DATE</pubDate>\n" +
                    "</item>";

    public static void main(String[] args) {
        SpringApplication.run(RssParserApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public CommandLineRunner demo(RestTemplate template) {
        return args -> {
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 1; i <= 28; i++) {
                System.out.println("Round: " + i);
                ResponseEntity<String> forEntity = template.getForEntity(
                        "https://echo.msk.ru/programs/victory/archive/" + i + ".html", String.class
                );
                String body = forEntity.getBody();
                Document document = Jsoup.parse(body);
                Elements elements = document.select("div.rel > div.iblock > div.prevcontent");
                for (Element element : elements) {
                    String title = element.select("p.txt > a.dark > strong.title").text();
                    String link = element.select("p.txt > a.dark").attr("href");
                    String downloadLink = element.select("div.mediamenu > a.download").attr("href");
                    String duration = element.select("div.mediamenu > a.listen > span.size").text();

                    System.out.println("Title: " + title);
                    System.out.println("Link: " + link);
                    System.out.println("Download: : " + downloadLink);
                    System.out.println("Duration: : " + duration);

                    if (StringUtils.isEmpty(downloadLink)) {
                        System.out.println("!!! Download url or url is empty");
                        continue;
                    }

                    title = StringUtils.isEmpty(title) ? element.select("a > strong.title").text() : title;
                    title = StringUtils.isEmpty(title) ? element.select("span.about > strong.name").text() : title;


                    Assert.hasText(title, "title must not be empty");
                    String date = downloadLink.substring(28, 38);
                    String[] len = duration.split(":");
                    int length = (Integer.valueOf(len[0]) * 60 + Integer.valueOf(len[1])) * 1000;


                    String result = TEMPLATE.replace("TITLE", title)
                            .replaceAll("DOWNLOAD_URL", downloadLink)
                            .replace("LINK", "https://echo.msk.ru" + link)
                            .replace("DURATION", duration)
                            .replace("LENGTH", String.valueOf(length))
                            .replace("DATE", date);
                    stringBuilder.append(result);
                }
            }
            System.out.println("-----------------------");
            System.out.println(stringBuilder.toString());
            System.out.println("-----------------------");
        };
    }
}
